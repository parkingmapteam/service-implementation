<?php


namespace Parkingmap\Wrapper\Event;

use Countable;
use Iterator;
use OutOfBoundsException;
use SeekableIterator;

class Events implements Countable, Iterator, SeekableIterator, \JsonSerializable
{

    private int $position;
    private array $events = [];

    public function __construct()
    {
        $this->position = 0;
    }

    public function add(EventInterface $event) : self
    {
        $this->events[] = $event;
        return $this;
    }

    public function count() : int
    {
        return count($this->events);
    }

    public function current(): EventInterface
    {
        if (isset($this->events[$this->position])) {
            return $this->events[$this->position];
        }
        throw new OutOfBoundsException();
    }

    public function next() : self
    {
        ++$this->position;

        return $this;
    }

    public function key() : int
    {
        return $this->position;
    }


    public function seek($position) : self
    {
        if ($position < 0 || $position > count($this->events)) {
            throw new OutOfBoundsException("position ${$position} is out of range");
        }
        $this->position = $position;
        return $this;
    }

    public function valid() : bool
    {
        return isset($this->events[$this->position]);
    }

    public function rewind() : self
    {
        $this->position = 0;
        return $this;
    }

    public function first()
    {
        $this->rewind()->current();
    }

    public function last()
    {
        $this->position = $this->count()>0 ? $this->count() - 1 : 0;
        return $this->current();
    }

    public function jsonSerialize() : array
    {
        $items = [];
        foreach ($this as $item) {
            $items[] = $item;
        }
        return $items;
    }
}