<?php

namespace Parkingmap\Wrapper\Event;

use DateTimeInterface;

class Event implements EventInterface
{
    protected  string $ref;
    protected DateTimeInterface $date;
    protected array $content;

    protected string $exchange;

    /**
     * Event constructor.
     * @param string $ref
     * @param DateTimeInterface $date
     * @param array $content
     */
    public function __construct(string $ref, DateTimeInterface $date, array $content)
    {
        $this->ref = $ref;
        $this->date = $date;
        $this->content = $content;
        $this->exchange = null;
    }

    public function getRef(): string
    {
        return $this->ref;
    }

    public function getDate(): DateTimeInterface
    {
        return $this->date;
    }

    public function getContent(): array
    {
        return $this->content;
    }

    public function setContent(array $content): void
    {
        $this->content = $content;
    }

    public function getExchange(): array
    {
        return $this->content;
    }

    public function setExchange(string $exchange): Event
    {
        $this->exchange = $exchange;
        return $this;
    }
}