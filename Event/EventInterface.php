<?php


namespace Parkingmap\Wrapper\Event;


use Parkingmap\Wrapper\Message\MessageInterface;
use DateTimeInterface;
interface EventInterface
{
    public function getDate(): DateTimeInterface;
    public function getRef(): string;
    public function getContent(): array;
    public function setContent(array $content): void;
}