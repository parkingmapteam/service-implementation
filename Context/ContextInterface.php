<?php


namespace Parkingmap\Wrapper\Context;

use Parkingmap\Wrapper\Event\EventInterface;
use Parkingmap\Wrapper\Item\ItemInterface;
use PhpAmqpLib\Message\AMQPMessage;
use Parkingmap\Wrapper\Service\Services;

interface ContextInterface
{

    public function __construct(EventInterface $event, ItemInterface $item, Services $services);

    public function getItem() : ItemInterface;

    public function getEvent() : EventInterface;

    public function getServices(): Services;
}