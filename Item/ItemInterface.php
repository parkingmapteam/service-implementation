<?php


namespace Parkingmap\Wrapper\Item;


use Parkingmap\Wrapper\Service\Services;

interface ItemInterface
{
    public function getDocumentId(): string;
    
    public function getRef(): string;

    public function getData(): array;

    public function getProperty(string $propertyKey);

    public function setProperty(string $propertyKey, $propertyValue): void;

    public function getInformation(string $informationKey);
}