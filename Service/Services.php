<?php


namespace Parkingmap\Wrapper\Service;

use Countable;
use Iterator;
use OutOfBoundsException;
use SeekableIterator;

class Services implements Countable, Iterator, SeekableIterator, \JsonSerializable
{

    private int $position;
    private array $items = [];

    public function __construct()
    {
        $this->position = 0;
    }

    public function add(ServiceInterface $service) : self
    {
        $this->items[] = $service;
        return $this;
    }

    public function count() : int
    {
        return count($this->items);
    }

    public function current(): ServiceInterface
    {
        if (isset($this->items[$this->position])) {
            return $this->items[$this->position];
        }
        throw new OutOfBoundsException("Index is out of range");
    }

    public function next() : self
    {
        ++$this->position;

        return $this;
    }

    public function key() : int
    {
        return $this->position;
    }


    public function seek($position) : self
    {
        if ($position < 0 || $position > count($this->items)) {
            throw new OutOfBoundsException("position ${$position} is out of range ");
        }
        $this->position = $position;
        return $this;
    }

    public function valid() : bool
    {
        return isset($this->items[$this->position]);
    }

    public function rewind() : self
    {
        $this->position = 0;
        return $this;
    }

    public function jsonSerialize() : array
    {
        $items = [];
        foreach ($this as $item) {
            $items[] = $item;
        }
        return $items;
    }
}